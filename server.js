var express = require("express");
var pug = require("pug");
var fs = require("fs");

try {
	var tabledata = JSON.parse(fs.readFileSync("tabledata.json", "utf8"));
} catch(error) {
	if (error.code == "ENOENT") {
		var tabledata = [];
	} else throw error;
}

var app = express();
app.set("view engine", "pug");
app.use(express.urlencoded());

app.get("/", (req,res) => {
	res.render("home", {
		title: "foo",
		enableForm: !tabledata.some(x => x.ia == req.ip),
		tabledata
	});
});

app.post("/submit", (req, res) => {
	console.log("form:", req.body);
	if (!req.body.username) return void res.status(400).send("At least username is required!");
	if (tabledata.some(x => x.ia == req.ip)) return void res.status(403).send("Sorry, only one entry per IP address!");
	var entry = {
		ia: req.ip,
		date: new Date(),
		username: req.body.username?.substr(0, 32),
		email: req.body.email?.substr(0, 39),
		website: req.body.website?.substr(0, 69),
		message: req.body.message?.substr(0, 169)
	};
	tabledata.unshift(entry);
	fs.writeFileSync("tabledata.json", JSON.stringify(tabledata));
	res.redirect("back");
})

app.use(express.static("public"));

void app.listen(8080);